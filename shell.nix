{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell { buildInputs = [ wget pipenv bzip2 ]; }
