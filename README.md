# Datathon

## Data sources

```shell
./download.sh
```

## Quick start

Requirements:

- Docker
- Python
- pipenv

```
docker run --name datathon_postgres -p 13339:5432 -e POSTGRES_PASSWORD=geheim -d postgis/postgis
pipenv install
pipenv shell
export FLASK_APP=app
flask db upgrade

# Start importing data
flask import_postcodes data/postcodes/de.csv
flask import_open_corporates data/open_corporates/de_companies_ocdata.jsonl
flask import_ted data/ted
```

## Example requests

```
curl http://localhost:5000/companies?name=Forschungszentrum
curl http://localhost:5000/companies/1220137
curl http://localhost:5000/persons?name=Dorothee%20Dzwonnek
curl http://localhost:5000/persons/805196

curl http://localhost:5000/queryies/persons_most_companies
curl http://localhost:5000/queryies/companies_hightes_tender_value_sum
```

## Tools

```
make test
make lint
make pretty
```
