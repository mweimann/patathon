import unittest

from unittest_data_provider import data_provider

from helpers import normalise_string, normalise_company_name


class TestNormalise(unittest.TestCase):

    strings = lambda: (
        ("Peter", "peter"),
        ("", ""),
        (1, "1"),
        ("Bärbel", "baerbel"),
        ("Fuß", "fuss"),
        ("äöüß", "aeoeuess"),
        ("h/a]l+l=o", "hallo"),
    )

    companies = lambda: (
        ("Ebbelwoi Gesellschaft mit begrenzter Haftung", "ebbelwoigmbh"),
        ("Handkäs mit Mussig Aktiengesellschaft", "handkaesmitmussigag"),
    )

    @data_provider(strings)
    def test_normalise_string(self, value, expected):
        self.assertEqual(normalise_string(value), expected)

    @data_provider(companies)
    def test_normalise_company_name(self, value, expected):
        self.assertEqual(normalise_company_name(value), expected)


if __name__ == "main":
    unittest.main()
