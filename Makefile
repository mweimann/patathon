$(shell mkdir -p tmp)

.PHONY: test
test: tmp/pipenv_token
	pipenv run python -m unittest discover test

.PHONY: lint
lint: tmp/pipenv_token
	pipenv run flake8

.PHONY: pretty
pretty: tmp/pipenv_token
	pipenv run yapf --in-place --recursive .

tmp/pipenv_token: Pipfile.lock
	pipenv install
	touch tmp/pipenv_token
