from sqlalchemy import Column, ForeignKey, Float, Integer, String
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry

from app import db


class Tender(db.Model):
    __tablename__ = "tender"
    # TODO Add bidders

    id = Column(Integer, primary_key=True)
    title = Column(String(300), nullable=True)
    country = Column(String(300), nullable=True)
    currency = Column(String(5), nullable=True)
    value = Column(Float, nullable=True)

    contracting_authority_id = Column(Integer, ForeignKey("company.id"))
    contracting_authority = relationship(
        "Company", backref="tenders_published", foreign_keys=[contracting_authority_id]
    )

    winner_id = Column(Integer, ForeignKey("company.id"))
    winner = relationship("Company", backref="tenders_won", foreign_keys=[winner_id])


class Person(db.Model):
    __tablename__ = "person"

    id = Column(Integer, primary_key=True)
    name = Column(String(200), index=True, nullable=False)

    companies = relationship("PersonCompany", back_populates="person")


class Company(db.Model):
    __tablename__ = "company"

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    number = Column(String(100), index=True, nullable=False)

    persons = relationship("PersonCompany", back_populates="company")


class PersonCompany(db.Model):
    __tablename__ = "person_company"

    person_id = Column(Integer, ForeignKey("person.id"), primary_key=True)
    company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)

    person = relationship("Person", back_populates="companies")
    company = relationship("Company", back_populates="persons")


class Postcode(db.Model):
    __tablename__ = "postcode"

    id = Column(Integer, primary_key=True)
    postcode = Column(String(5), nullable=False, unique=True)
    name = Column(String(100), nullable=False)
    coordinates = Column(Geometry("POINT"))
