#!/usr/bin/env bash

# postcodes
wget -O ./data/postcodes/de.csv https://launix.de/launix/wp-content/uploads/2019/06/PLZ.csv

# offeneregister.de
# download
wget -O ./data/open_corporates/de_companies_ocdata.jsonl.bz2 https://daten.offeneregister.de/de_companies_ocdata.jsonl.bz2

# extract
pushd ./data/open_corporates
bzip2 -dk de_companies_ocdata.jsonl.bz2
popd

# ted
# download
wget -O ./data/ted/2022-01.tar.gz https://ted.europa.eu/xml-packages/monthly-packages/2022/2022-01.tar.gz
wget -O ./data/ted/2022-02.tar.gz https://ted.europa.eu/xml-packages/monthly-packages/2022/2022-02.tar.gz

# extract
pushd ./data/ted
tar -xzf 2022-01.tar.gz
tar -xzf 2022-02.tar.gz
popd
