from flask import request
from sqlalchemy import select, func, text

from app import app, db
from models import Company, Person, PersonCompany, Tender
from api_schemas import CompanyApiSchema, PersonApiSchema, TenderApiSchema


@app.route("/companies")
def find_companies():
    name_param = request.args.get("name")

    if not name_param:
        return {}

    companies = Company.query.filter(
        Company.name.ilike(f"{name_param}%")).limit(25).all()
    company_api_schema = CompanyApiSchema(many=True)
    return {"companies": company_api_schema.dump(companies)}


@app.route("/companies/<id>")
def get_company(id):
    company = Company.query.get(id)

    if not company:
        return "not found\n", 404

    persons = []

    for person_company in company.persons:
        persons.append(person_company.person)

    tenders = Tender.query.filter(
        Tender.contracting_authority_id == company.id).all()
    contracts = Tender.query.filter(Tender.winner_id == company.id).all()

    company_api_schema = CompanyApiSchema()
    persons_api_schema = PersonApiSchema(many=True)
    tenders_api_schema = TenderApiSchema(many=True)
    return {
        "company": company_api_schema.dump(company),
        "persons": persons_api_schema.dump(persons),
        "tenders": tenders_api_schema.dump(tenders),
        "contracts": tenders_api_schema.dump(contracts)
    }


@app.route("/persons")
def find_persons():
    name_param = request.args.get("name")

    if not name_param:
        return {}

    persons = Person.query.filter(
        Person.name.ilike(f"{name_param}%")).limit(25).all()
    company_api_schema = PersonApiSchema(many=True)
    return {"persons": company_api_schema.dump(persons)}


@app.route("/persons/<id>")
def get_person(id):
    person = Person.query.get(id)

    if not person:
        return "not found\n", 404

    companies = []

    for person_company in person.companies:
        companies.append(person_company.company)

    company_api_schema = CompanyApiSchema(many=True)
    person_api_schema = PersonApiSchema()
    return {
        "person": person_api_schema.dump(person),
        "companies": company_api_schema.dump(companies)
    }


@app.route("/queryies/persons_most_companies")
def find_persons_with_most_companies():
    statement = select(Person,
                       func.count("*").label("cnt")).join(
                           PersonCompany,
                           Person.id == PersonCompany.person_id).group_by(
                               Person.id).order_by(text("cnt DESC")).limit(25)
    db_results = db.session.execute(statement).all()

    person_api_schema = PersonApiSchema()
    results = []

    for (person, count) in db_results:
        results.append({
            "person": person_api_schema.dump(person),
            "companies": count
        })

    return {"persons": results}


@app.route("/queryies/companies_hightes_tender_value_sum")
def find_companies_with_highest_tender_amount_sum():
    statement = select(
        Company,
        func.sum(Tender.value).label("sum")).join(
            Company, Company.id == Tender.contracting_authority_id).filter(
                Tender.contracting_authority_id != None).filter(
                    Tender.value != None).group_by(Company.id).order_by(
                        text("sum DESC")).limit(25)
    db_results = db.session.execute(statement).all()

    tender_api_schema = TenderApiSchema()
    results = []

    for (company, value_sum) in db_results:
        results.append({
            "company": tender_api_schema.dump(company),
            "tenders_value_sum": value_sum
        })

    return {"companies": results}
