from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)

app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:geheim@localhost:13339/postgres"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db, compare_type=True)
ma = Marshmallow(app)

from models import Person, Company, PersonCompany, Postcode, Tender
import commands  # noqa
import routes  # noqa


class TenderSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Tender

    id = ma.auto_field()
    title = ma.auto_field()
    currency = ma.auto_field()
    value = ma.auto_field()
    contracting_authority = ma.auto_field()
    winner = ma.auto_field()

tenders_schema = TenderSchema(many=True)

@app.route('/tender/<query>', methods=['GET'])
def tender_query(query):
    tenders = Tender.query.filter(Tender.title.like('%' + query + '%')).all()
    print(tenders_schema.dump(tenders))
    return dict(tenders_schema.dump(tenders))
